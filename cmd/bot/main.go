package main

import (
	"fmt"
	"os"

	"gitlab.com/wildestgarlic/tfm/internal/domain/application"
)

//nolint:gochecknoglobals // build var
var appVersion = "v0.0.1"

func main() {
	app := application.New(appVersion)

	defer app.Shutdown()

	if err := app.Run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}
