FROM golang:1.23-alpine3.20 AS modules

COPY go.mod go.sum /modules/

WORKDIR /modules

RUN go mod download

FROM golang:1.23-alpine3.20 AS builder

ARG CI_COMMIT_TAG

RUN apk --no-cache add ca-certificates

COPY --from=modules /go/pkg /go/pkg

COPY . /bot

WORKDIR /bot

RUN \
  CGO_ENABLED=0 GOOS=linux GOARCH=amd64 \
  go build  \
    -ldflags="-w -s" \
    -ldflags="-X main.appVersion=$CI_COMMIT_TAG" \
    -o /bin/bot ./cmd/bot

FROM scratch

COPY --from=builder /bin/bot /bot
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

CMD ["/bot"]
