include .env
export

check: generate format lint test

lint:
	golangci-lint run --config .golangci.yml

run:
	go run cmd/bot/main.go

generate:
	find . -name '*.gen.go' | xargs rm || true
	go generate ./...

test:
	go test ./...

format:
	gofumpt -l -w -extra .
	gci write --skip-generated -s standard -s default -s "prefix(gitlab.com/wildestgarlic)" .
