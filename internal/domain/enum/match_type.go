package enum

type MatchType int64

const (
	MatchTypeRegexp MatchType = iota + 1
	MatchTypePrefix
)

func (t MatchType) Is(another MatchType) bool {
	return t == another
}
