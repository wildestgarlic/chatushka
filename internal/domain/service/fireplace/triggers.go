package fireplace

import (
	"regexp"

	"gitlab.com/wildestgarlic/tfm/internal/domain/value"
)

//nolint:gochecknoglobals
var (
	TriggerBob = value.NewPrefixTrigger(
		"Боб,",
		value.NewPercentChance(100), //nolint:mnd // bebebe
	)
	TriggerChillRequest = value.NewRegexTrigger(
		regexp.MustCompile(`(?mi)чил.*\?\)*`),
		value.NewPercentChance(25), //nolint:mnd // bebebe
	)
	TriggerPhilosophy = value.NewRegexTrigger(
		regexp.MustCompile(`(?mi)(это.*много|неужели|неужто|разве)\?\)*`),
		value.NewPercentChance(15), //nolint:mnd // bebebe
	)
	TriggerEightBall = value.NewRegexTrigger(
		regexp.MustCompile(`(?mi).*\?\)*`),
		value.NewPercentChance(25), //nolint:mnd // bebebe
	)
	TriggerMustEightBall = value.NewRegexTrigger(
		regexp.MustCompile(`(?mi)(да|нет).?\?\)*`),
		value.NewPercentChance(100), //nolint:mnd // bebebe
	)
	TriggerWeather = value.NewRegexTrigger(
		regexp.MustCompile(`(?mi)погод[аы].*\?\)*`),
		value.NewPercentChance(15), //nolint:mnd // bebebe
	)
	TriggerGrowsPoint = value.NewRegexTrigger(
		regexp.MustCompile(`(?mi)^это.*\?\)*`),
		value.NewPercentChance(25), //nolint:mnd // bebebe
	)
	TriggerSupport = value.NewRegexTrigger(
		regexp.MustCompile(`(?mi)(сильно|мудро|тупо|солас).*\?\)*`),
		value.NewPercentChance(25), //nolint:mnd // bebebe
	)
	TriggerOk = value.NewRegexTrigger(
		regexp.MustCompile(`(?mi)^(ясно|понятно).?$`),
		value.NewPercentChance(15), //nolint:mnd // bebebe
	)
	TriggerThanks = value.NewRegexTrigger(
		regexp.MustCompile(`(?mi)(благодарю|спасибо)`),
		value.NewPercentChance(25), //nolint:mnd // bebebe
	)
)
