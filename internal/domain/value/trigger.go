package value

import (
	"math/rand"
	"regexp"
	"strings"

	"gitlab.com/wildestgarlic/tfm/internal/domain/enum"
)

type Trigger struct {
	mathType enum.MatchType
	chance   Chance
	text     string
	regexp   *regexp.Regexp
}

func NewPrefixTrigger(text string, chance Chance) Trigger {
	return Trigger{
		mathType: enum.MatchTypePrefix,
		chance:   chance,
		text:     text,
	}
}

func NewRegexTrigger(re *regexp.Regexp, chance Chance) Trigger {
	return Trigger{
		mathType: enum.MatchTypeRegexp,
		chance:   chance,
		regexp:   re,
	}
}

func (t Trigger) Match(text string) bool {
	switch t.mathType {
	case enum.MatchTypePrefix:
		return strings.HasPrefix(text, t.text)
	case enum.MatchTypeRegexp:
		return t.regexp.MatchString(text)
	}

	return false
}

func (t Trigger) LookChance(source *rand.Rand) bool {
	return t.chance.Look(source)
}
