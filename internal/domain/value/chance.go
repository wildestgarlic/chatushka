package value

import (
	"math/rand"
)

type Chance struct {
	n, of int
}

func NewPercentChance(n int) Chance {
	const maximum = 100

	if n > maximum {
		panic("chance: n > maximum")
	}

	return Chance{
		n:  n,
		of: maximum,
	}
}

func (c Chance) N() int                      { return c.n }
func (c Chance) Of() int                     { return c.of }
func (c Chance) Look(source *rand.Rand) bool { return source.Intn(c.of) >= c.n }
