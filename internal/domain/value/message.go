package value

type Message string

func (m Message) String() string {
	return string(m)
}
