package application

import (
	"fmt"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"

	"gitlab.com/wildestgarlic/tfm/internal/config"
	"gitlab.com/wildestgarlic/tfm/internal/domain/component"
	"gitlab.com/wildestgarlic/tfm/internal/domain/server"
	"gitlab.com/wildestgarlic/tfm/internal/domain/service/fireplace"
)

type Application struct {
	appVersion       string
	cfg              config.Config
	messageResponder component.MessageResponder
	server           server.Server
	botAPI           *tgbotapi.BotAPI
}

func New(appVersion string) Application {
	return Application{appVersion: appVersion}
}

func (app *Application) Run() error {
	var err error

	app.cfg, err = config.Load()
	if err != nil {
		return fmt.Errorf("config.Load: %w", err)
	}

	if err := app.createBotAPI(); err != nil {
		return fmt.Errorf("createBotAPI: %w", err)
	}

	app.createMessageResponder()
	app.createServer()

	if err = app.server.Start(); err != nil {
		return fmt.Errorf("server.Start: %w", err)
	}

	return nil
}

func (app *Application) createBotAPI() (err error) {
	app.botAPI, err = tgbotapi.NewBotAPI(app.cfg.BotToken)
	if err != nil {
		return fmt.Errorf("tgbotapi.NewBotAPI: %w", err)
	}

	if app.cfg.BotDebug {
		app.botAPI.Debug = true
	}

	return
}

func (app *Application) createServer() {
	app.server = server.New(app.botAPI, app.messageResponder)
}

func (app *Application) createMessageResponder() {
	responder := component.NewMessageResponder()

	responder.Add(fireplace.TriggerBob, fireplace.MessagesTriggerBob)
	responder.Add(fireplace.TriggerChillRequest, fireplace.MessagesChillRequest)
	responder.Add(fireplace.TriggerPhilosophy, fireplace.MessagesPhilosophy)
	responder.Add(fireplace.TriggerEightBall, fireplace.MessagesEightBall)
	responder.Add(fireplace.TriggerMustEightBall, fireplace.MessagesEightBall)
	responder.Add(fireplace.TriggerWeather, fireplace.MessagesWeather)
	responder.Add(fireplace.TriggerGrowsPoint, fireplace.MessagesGrowsPoint)
	responder.Add(fireplace.TriggerSupport, fireplace.MessagesSupport)
	responder.Add(fireplace.TriggerOk, fireplace.MessagesOk)
	responder.Add(fireplace.TriggerThanks, fireplace.MessagesThanks)

	app.messageResponder = responder
}

func (app *Application) Shutdown() {
	app.server.Shutdown()
}
