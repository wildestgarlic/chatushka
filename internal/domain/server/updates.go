package server

import (
	"fmt"
	"log"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"

	"gitlab.com/wildestgarlic/tfm/internal/domain/component"
)

type Server struct {
	bot              *tgbotapi.BotAPI
	messageResponder component.MessageResponder
}

func New(
	bot *tgbotapi.BotAPI,
	messageResponder component.MessageResponder,
) Server {
	return Server{
		bot:              bot,
		messageResponder: messageResponder,
	}
}

func (b *Server) Start() error {
	log.Printf("Authorized on account: %s", b.bot.Self.UserName)

	updates, err := b.initUpdatesChannel()
	if err != nil {
		return fmt.Errorf("init updates channel: %w", err)
	}

	b.handleUpdates(updates)

	return nil
}

func (b *Server) handleUpdates(updates tgbotapi.UpdatesChannel) {
	for update := range updates {
		if update.Message.NewChatMembers != nil && len(*update.Message.NewChatMembers) != 0 {
			b.handleNewUsers(update.Message.Chat.ID, *update.Message.NewChatMembers)
		}

		if update.Message == nil {
			continue
		}

		if update.Message.IsCommand() {
			b.handleCommand(*update.Message)

			continue
		}

		go b.handleMessage(update.Message.Chat.ID, update.Message.Text)
	}
}

func (b *Server) initUpdatesChannel() (tgbotapi.UpdatesChannel, error) {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 120

	return b.bot.GetUpdatesChan(u)
}

func (b *Server) Shutdown() {
	b.bot.StopReceivingUpdates()
}
