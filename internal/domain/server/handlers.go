package server

import (
	"fmt"
	"log"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"

	"gitlab.com/wildestgarlic/tfm/internal/domain/service/fireplace"
)

func (b *Server) sendMessage(chatID int64, text string) {
	msg := tgbotapi.NewMessage(chatID, text)

	if _, err := b.bot.Send(msg); err != nil {
		log.Printf("bot.Send: %s", err)
	}
}

func (b *Server) handleCommand(message tgbotapi.Message) {
	switch message.Command() { //nolint:gocritic // future
	case CommandStart:
		b.handleStart(message.Chat.ID)
	}
}

func (b *Server) handleMessage(chatID int64, message string) {
	if firewood, ok := b.messageResponder.Generate(message); ok {
		log.Printf("%s: %s\n", message, firewood)
		b.sendMessage(chatID, firewood.String())
	}
}

func (b *Server) handleStart(chatID int64) {
	b.sendMessage(chatID, fireplace.StartMessage)
}

func (b *Server) handleNewUsers(chatID int64, users []tgbotapi.User) {
	for _, user := range users {
		if user.IsBot {
			b.sendMessage(chatID, fmt.Sprintf("Привет, %s, ты тоже бот?", user.UserName))
			continue
		}

		b.sendMessage(chatID, fmt.Sprintf("Добро пожаловать, %s!", user.FirstName))
	}
}
