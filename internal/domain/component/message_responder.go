package component

import (
	"math/rand"
	"time"

	"gitlab.com/wildestgarlic/tfm/internal/domain/value"
)

type MessageResponder struct {
	rand *rand.Rand
	t2m  map[value.Trigger][]value.Message
}

func NewMessageResponder() MessageResponder {
	const preallocate = 10

	return MessageResponder{
		rand: rand.New(rand.NewSource(time.Now().UnixNano())), //nolint:gosec // no security
		t2m:  make(map[value.Trigger][]value.Message, preallocate),
	}
}

func (m MessageResponder) Add(
	trigger value.Trigger,
	messages []value.Message,
) {
	if len(messages) == 0 {
		panic("empty messages not allowed")
	}

	m.t2m[trigger] = messages
}

func (m MessageResponder) Generate(message string) (value.Message, bool) {
	for trigger, responses := range m.t2m {
		if trigger.Match(message) && trigger.LookChance(m.rand) {
			return m.messageOf(responses), true
		}
	}

	return "", false
}

func (m MessageResponder) messageOf(messages []value.Message) value.Message {
	return messages[m.rand.Intn(len(messages))]
}
