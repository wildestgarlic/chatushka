package config

import (
	"fmt"

	"github.com/caarlos0/env/v6"
)

// Load loads environment variables to Config.
func Load() (Config, error) {
	var config Config

	if err := env.Parse(&config); err != nil {
		return Config{}, fmt.Errorf("env.Parse: %w", err)
	}

	return config, nil
}

// Config holds application config variables.
type Config struct {
	BotToken string `env:"BOT_TOKEN"`
	BotDebug bool   `env:"BOT_DEBUG" env-default:"false"`
	LibreURL string `env:"LIBRE_URL"`
}
