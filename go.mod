module gitlab.com/wildestgarlic/tfm

go 1.23

require (
	github.com/caarlos0/env/v6 v6.10.1
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
)

require github.com/technoweenie/multipartstreamer v1.0.1 // indirect
